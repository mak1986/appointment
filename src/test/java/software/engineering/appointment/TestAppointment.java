package software.engineering.appointment;

import java.text.ParseException;
import org.junit.Test;

public class TestAppointment {
	
	@Test(expected=ParseException.class)
	public void testAppointment() throws ParseException {
		new Appointment("05-09-2014 14:18", "Study Software Engineering");
	}

}
