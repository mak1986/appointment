package software.engineering.appointment;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

public class TestOneTime {

	private OneTime oneTimeAppointment;
	
	@Before
	public void setup() throws ParseException{
		oneTimeAppointment = new OneTime("1/1/14 12:00", "a description");
	}
	@Test
	public void testOneTimeOccursOnSameDate() throws ParseException {
		assertTrue(oneTimeAppointment.occursOn(2014, 1, 1));
	}
	@Test
	public void testOneTimeOccursOnBeforeDate() throws ParseException {
		assertFalse(oneTimeAppointment.occursOn(2013, 12, 31));
	}
	@Test
	public void testOneTimeOccursOnAfterOneDay() throws ParseException {
		assertFalse(oneTimeAppointment.occursOn(2014, 1, 2));
	}
	@Test
	public void testOneTimeOccursOnAfterOneWeek() throws ParseException {
		assertFalse(oneTimeAppointment.occursOn(2014, 1, 8));
	}
	@Test
	public void testOneTimeOccursOnAfterOneMonth() throws ParseException {
		assertFalse(oneTimeAppointment.occursOn(2014, 2, 1));
	}
}
