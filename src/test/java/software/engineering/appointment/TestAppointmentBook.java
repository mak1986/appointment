package software.engineering.appointment;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

public class TestAppointmentBook {
	private AppointmentBook appointmentBook;
	private Appointment appointment1;
	private Appointment appointment2;
	private Appointment appointment3;
	@Before
	public void setup() throws ParseException{
		this.appointmentBook = new AppointmentBook();
		this.appointment1 = new Appointment("05/09/2014 14:18", "Study Software Engineering");
		this.appointment2 = new Appointment("08/09/2014 08:00", "System Analysis and design");
		this.appointment3 = new Appointment("09/09/2014 08:30", "Network");
		appointmentBook.add(appointment1);
		appointmentBook.add(appointment2);
		appointmentBook.add(appointment3);
	}
	@Test
	public void testAppointmentBookGetNumAppointments() {
		assertEquals(3,this.appointmentBook.getNumAppointments());
	}
	@Test
	public void testAppointmentBookGetAppointment(){
		assertEquals(this.appointment1,this.appointmentBook.getAppointment(0));
		assertEquals(this.appointment2,this.appointmentBook.getAppointment(1));
		assertEquals(this.appointment3,this.appointmentBook.getAppointment(2));
	}
	@Test
	public void testAppointmentBookToString(){
		System.out.println(this.appointmentBook.toString().length());
		assertEquals(238,this.appointmentBook.toString().length());
	}

}
