package software.engineering.appointment;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.TimeZone;

import org.junit.Before;
import org.junit.Test;

public class TestWeekly {

	private Weekly weeklyAppointment;

	@Before
	public void setUp() throws Exception {
		weeklyAppointment = new Weekly("1/1/14 12:00", "a description");
	}

	@Test
	public void testWeeklyOccursOnSameDate() throws ParseException {
		assertTrue(weeklyAppointment.occursOn(2014, 1, 1));
	}
	@Test
	public void testWeeklyOccursOnBeforeOneDay() throws ParseException {
		assertFalse(weeklyAppointment.occursOn(2013, 12, 31));
	}
	@Test
	public void testWeeklyOccursOnBeforeOneWeek() throws ParseException {
		assertFalse(weeklyAppointment.occursOn(2013, 12, 25));
	}
	@Test
	public void testWeeklyOccursOnAfterOneDay() throws ParseException {
		assertFalse(weeklyAppointment.occursOn(2014, 1, 2));
	}
	@Test
	public void testWeeklyOccursOnAfterOneWeek() throws ParseException {
		assertTrue(weeklyAppointment.occursOn(2014, 1, 8));
	}

}
