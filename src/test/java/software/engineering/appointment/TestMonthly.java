package software.engineering.appointment;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

public class TestMonthly {

private Monthly monthlyAppointment;
	
	@Before
	public void setup() throws ParseException{
		monthlyAppointment = new Monthly("1/1/14 12:00", "a description");
	}
	@Test
	public void testMonthlyOccursOnSameDate() throws ParseException {
		assertTrue(monthlyAppointment.occursOn(2014, 1, 1));
	}
	@Test
	public void testMonthlyOccursOnBeforeDate() throws ParseException {
		assertFalse(monthlyAppointment.occursOn(2013, 12, 31));
	}
	@Test
	public void testMonthlyOccursOnAfterOneDay() throws ParseException {
		assertFalse(monthlyAppointment.occursOn(2014, 1, 2));
	}
	@Test
	public void testMonthlyOccursOnAfterOneWeek() throws ParseException {
		assertFalse(monthlyAppointment.occursOn(2014, 1, 8));
	}
	@Test
	public void testMonthlyOccursOnAfterOneMonth() throws ParseException {
		assertTrue(monthlyAppointment.occursOn(2014, 2, 1));
	}
	@Test
	public void testMonthlyOccursOnAfterTwoMonth() throws ParseException {
		assertTrue(monthlyAppointment.occursOn(2014, 3, 1));
	}
}
