package software.engineering.appointment;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

public class TestDaily {

	private Daily dailyAppointment;
	
	@Before
	public void setup() throws ParseException{
		dailyAppointment = new Daily("1/1/14 12:00", "a description");
	}
	@Test
	public void testDailyOccursOnSameDate() throws ParseException {
		assertTrue(dailyAppointment.occursOn(2014, 1, 1));
	}
	@Test
	public void testDailyOccursOnBeforetDate() throws ParseException {
		assertFalse(dailyAppointment.occursOn(2013, 12, 31));
	}
	@Test
	public void testDailyOccursOnAfterDate() throws ParseException {
		assertTrue(dailyAppointment.occursOn(2014, 1, 2));
	}
}
