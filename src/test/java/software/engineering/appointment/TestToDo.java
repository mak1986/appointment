package software.engineering.appointment;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

public class TestToDo {

	private ToDo toDoWithDate;
	private ToDo toDoWithOutDate;
	
	@Before
	public void setUp() throws Exception {
		this.toDoWithDate = new ToDo("22/08/14", "todo 1");
		this.toDoWithOutDate = new ToDo("todo 2");
	}

	@Test
	public void testToDoOccursOnWithDateTrue() throws ParseException {
		assertTrue(this.toDoWithDate.occursOn(2014, 8, 22));
	}
	@Test
	public void testToDoOccursOnWithDateFalse() throws ParseException {
		assertFalse(this.toDoWithDate.occursOn(2014, 8, 23));
	}
	@Test
	public void testToDoOccursOnWithOutDate() throws ParseException {
		assertTrue(this.toDoWithOutDate.occursOn(2014, 8, 22));
	}
}
