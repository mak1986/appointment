package software.engineering.appointment;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestAppointment.class, TestAppointmentBook.class,
		TestDaily.class, TestMonthly.class, TestOneTime.class, TestWeekly.class, TestToDo.class })
public class AllTests {

}
