package software.engineering.appointment;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.ParseException;
import java.util.Comparator;

import javax.swing.SwingConstants;
import javax.swing.JScrollPane;

import java.awt.Font;

import javax.swing.JButton;

public class GUI extends JFrame implements ActionListener{

	private Manager manager;
	
	private JTextField dateTimetextField;
	private JTextArea messageTextArea;
	private JTextArea calendarTextArea;
	private JComboBox<String> appointmentTypeComboBox;
	private JButton addEventBtn;

	private JRadioButton rdbtnAppointment;

	private JRadioButton rdbtnToDo;

	private JTextArea descriptionTextArea;

	private JButton saveBtn;
	private JButton showAllBtn;
	private JTextField searchTextField;
	private JButton searchBtn;

	public GUI() {
		this.setup();
		this.createSeperators();
		this.createEventType();
		this.createAppointmentType();
		this.createDateAndTime();
		this.createMessageTextArea();
		this.createCalendarTextArea();
		this.createDescriptionTextArea();
		this.createAddBtn();
		this.createSaveBtn();
		this.createShowAllBtn();
		this.createSearch();
	}
	private void setup() {
		setResizable(false);
		getContentPane().setLayout(null);
		setSize(610, 290);
		setTitle("Calendar");
		setVisible(true);
		try {
			this.manager = new Manager(new FileSource());
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void createSearch() {
		searchTextField = new JTextField();
		searchTextField.setBounds(317, 10, 110, 23);
		getContentPane().add(searchTextField);
		searchTextField.setColumns(10);
		
		searchBtn = new JButton("Search");
		searchBtn.setBounds(428, 10, 81, 23);
		searchBtn.setActionCommand("searchBtn");
		searchBtn.addActionListener(this);
		getContentPane().add(searchBtn);
	}
	private void createShowAllBtn() {
		showAllBtn = new JButton("Show all");
		showAllBtn.setBounds(510, 10, 86, 23);
		showAllBtn.setActionCommand("showAllBtn");
		showAllBtn.addActionListener(this);
		getContentPane().add(showAllBtn);
	}
	private void createSaveBtn() {
		saveBtn = new JButton("Save file");
		saveBtn.setBounds(317, 228, 86, 23);
		saveBtn.setActionCommand("saveBtn");
		saveBtn.addActionListener(this);
		getContentPane().add(saveBtn);
	}
	private void createDescriptionTextArea() {
		JLabel lblDescription = new JLabel("Description:");
		lblDescription.setBounds(11, 86, 105, 20);
		getContentPane().add(lblDescription);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(121, 86, 154, 41);
		getContentPane().add(scrollPane);
		
		descriptionTextArea = new JTextArea();
		descriptionTextArea.setFont(new Font("Calibri", Font.PLAIN, 11));
		descriptionTextArea.setLineWrap(true);
		descriptionTextArea.setWrapStyleWord(true);
		scrollPane.setViewportView(descriptionTextArea);
	}
	private void createAddBtn() {
		addEventBtn = new JButton("Add event");
		addEventBtn.setBounds(121, 138, 101, 23);
		addEventBtn.setActionCommand("addEventBtn");
		addEventBtn.addActionListener(this);
		getContentPane().add(addEventBtn);
	}

	private void createCalendarTextArea() {
		calendarTextArea = new JTextArea();
		calendarTextArea.setLineWrap(true);
		calendarTextArea.setWrapStyleWord(true);
		calendarTextArea.setFont(new Font("Calibri", Font.PLAIN, 11));
		calendarTextArea.setEditable(false);
		calendarTextArea.setForeground(Color.GREEN);
		calendarTextArea.setBackground(Color.DARK_GRAY);
		calendarTextArea.setBounds(0, 0, 4, 22);

		this.showAllEvents();
		
		JScrollPane scrollPane = new JScrollPane(calendarTextArea);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(318, 36, 278, 187);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		getContentPane().add(scrollPane);
	}
	private void createMessageTextArea() {
		messageTextArea = new JTextArea();
		messageTextArea.setFont(new Font("Calibri", Font.PLAIN, 11));
		messageTextArea.setEditable(false);
		messageTextArea.setForeground(Color.WHITE);
		messageTextArea.setBackground(Color.DARK_GRAY);
		messageTextArea.setBounds(10, 187, 264, 64);
		getContentPane().add(messageTextArea);
	}
	private void createSeperators() {
		JSeparator separator_1 = new JSeparator();
		separator_1.setOrientation(SwingConstants.VERTICAL);
		separator_1.setBounds(294, 11, 13, 240);
		getContentPane().add(separator_1);
		
		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(11, 172, 264, 4);
		getContentPane().add(separator_2);
	}
	private void createDateAndTime() {
		JLabel lblDateAndTime = new JLabel("Date and Time:");
		lblDateAndTime.setBounds(10, 61, 105, 20);
		getContentPane().add(lblDateAndTime);
		
		dateTimetextField = new JTextField();
		dateTimetextField.setFont(new Font("Calibri", Font.PLAIN, 11));
		dateTimetextField.setBounds(121, 61, 154, 20);
		getContentPane().add(dateTimetextField);
		dateTimetextField.setColumns(10);
	}
	private void createAppointmentType() {
		JLabel lblAppointmentType = new JLabel("Appointment type:");
		lblAppointmentType.setBounds(11, 36, 105, 20);
		getContentPane().add(lblAppointmentType);
		
		appointmentTypeComboBox = new JComboBox<String>();
		appointmentTypeComboBox.setModel(new DefaultComboBoxModel<String>(new String[] {"Daily", "Weekly", "Monthly", "One Time"}));
		appointmentTypeComboBox.setBounds(121, 36, 154, 20);
		appointmentTypeComboBox.setSelectedItem("Daily");
		getContentPane().add(appointmentTypeComboBox);
	}
	private void createEventType() {
		JLabel lblEventType = new JLabel("Event type:");
		lblEventType.setBounds(11, 11, 105, 20);
		getContentPane().add(lblEventType);
		

		rdbtnAppointment = new JRadioButton("Appointment");
		rdbtnAppointment.setBounds(121, 11, 101, 20);
		rdbtnAppointment.setActionCommand("appointmentBtn");
		rdbtnAppointment.addActionListener(this);
		getContentPane().add(rdbtnAppointment);
		
		rdbtnToDo = new JRadioButton("To do");
		rdbtnToDo.setBounds(220, 11, 69, 20);
		rdbtnToDo.setActionCommand("toDoBtn");
		rdbtnToDo.addActionListener(this);
		getContentPane().add(rdbtnToDo);
		
		ButtonGroup bG = new ButtonGroup();
		bG.add(rdbtnAppointment);
		bG.add(rdbtnToDo);
		
		rdbtnAppointment.setSelected(true);
	}

	
	private void showAllEvents(){
		calendarTextArea.setText(this.manager.getEventList().toString());
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		messageTextArea.setText("");
		if(e.getActionCommand().equals("toDoBtn")){
			appointmentTypeComboBox.setEnabled(false);;
		}else if(e.getActionCommand().equals("appointmentBtn")){
			appointmentTypeComboBox.setEnabled(true);;
		}else if(e.getActionCommand().equals("addEventBtn")){
			
			if(rdbtnToDo.isSelected()){
				this.addToDo();
			}else{
				this.addAppointment();
			}
		}else if(e.getActionCommand().equals("saveBtn")){
			manager.saveEvents();
			messageTextArea.setText("-> calendar.xml file has been saved.");
		}else if(e.getActionCommand().equals("showAllBtn")){
			calendarTextArea.setText(manager.getEventList().toString());
			messageTextArea.setText("-> Showing all events.");
		}else if(e.getActionCommand().equals("searchBtn")){
			try {
				calendarTextArea.setText(manager.getEventsOn(searchTextField.getText()).toString());
				messageTextArea.setText("-> Showing events on "+searchTextField.getText());
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
	}

	private void addAppointment() {
		try {
			manager.addAppointment(dateTimetextField.getText(), descriptionTextArea.getText(), appointmentTypeComboBox.getSelectedItem().toString());
			this.updateCalendarTextArea();
		} catch (ParseException e1) {
			e1.printStackTrace();
		}	
	}
	private void addToDo() {
		if(dateTimetextField.getText().length()>0){
			try {
				manager.addToDo(dateTimetextField.getText(), descriptionTextArea.getText());
				this.updateCalendarTextArea();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}else{
			try {
				manager.addToDo(descriptionTextArea.getText());
				this.updateCalendarTextArea();
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		}
		
	}
	private void updateCalendarTextArea() {
		calendarTextArea.setText(manager.getEventList().toString());
	}
	public static void main(String[] args){
		GUI ui = new GUI();
		GUIOrder guiOrderByDate = new GUIOrder("Event ordered by date",new Comparator<Event>(){
			@Override
			public int compare(Event e1, Event e2) {
				return e1.getDate().compareTo(e2.getDate());				
			}
		});
		GUIOrder guiOrderByDescription = new GUIOrder("Event ordered by description",new Comparator<Event>(){
			@Override
			public int compare(Event e1, Event e2) {
				return e1.getDescription().compareToIgnoreCase(e2.getDescription());
			}
		});
		

		ui.manager.addObserver(guiOrderByDate);
		ui.manager.addObserver(guiOrderByDescription);
	}


}
