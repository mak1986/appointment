package software.engineering.appointment;

import java.util.List;

public interface DataSource {
	abstract public List<Event> getEvents();
	abstract public void writeEvents(List<Event> events);
}
