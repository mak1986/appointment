package software.engineering.appointment;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.lang.reflect.Constructor;

public class FileSource implements DataSource{
	
	@Override
	public ArrayList<Event> getEvents() {
		
		ArrayList<Event> events = new ArrayList<Event>();
		
		try {
			File fXmlFile = new File("calendar.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);

			// optional, but recommended
			// read this -
			// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("event");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;
					String date = eElement.getElementsByTagName("date").item(0).getTextContent();
					String description = eElement.getElementsByTagName("description").item(0).getTextContent();
					String type = eElement.getElementsByTagName("type").item(0).getTextContent();
					type = type.replace(" ", "");
					
					Constructor c = Class.forName("software.engineering.appointment."+type).getConstructor(String.class, String.class);
					Event event = (Event) c.newInstance(date, description);
					events.add(event);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}

	@Override
	public void writeEvents(List<Event> events) {

		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
	 
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("events");
			doc.appendChild(rootElement);
			
			for(int i=0;i<events.size();i++){
				Element event = doc.createElement("event");
				rootElement.appendChild(event);
				Element type = doc.createElement("type");
				Event e = events.get(i);
				String cls = e.getClass().getSimpleName();
				type.appendChild(doc.createTextNode(cls));
				event.appendChild(type);
				
				Element date = doc.createElement("date");
				date.appendChild(doc.createTextNode(e.getFormatedDate()));
				event.appendChild(date);
				
				Element description = doc.createElement("description");
				description.appendChild(doc.createTextNode(e.getDescription()));
				event.appendChild(description);
			}
	 

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File("calendar.xml"));
	 

	 
			transformer.transform(source, result);
	 
			System.out.println("File saved!");
	 
		  } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
		
	}

}