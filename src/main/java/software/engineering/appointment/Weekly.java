package software.engineering.appointment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Weekly extends Appointment {

	public Weekly(String aDate, String aDescription) throws ParseException {
		super(aDate, aDescription);
	}

	@Override
	public boolean occursOn(int year, int month, int day) throws ParseException {
		if(super.occursOn(year, month, day)){	
			DateFormat DAY = new SimpleDateFormat("u",new Locale("en","US"));
			if(DAY.format(this.getDateNoTime()).equals(DAY.format(DATEFORMAT.parse(day+"/"+month+"/"+year)))){
				return true;
			}else{
				return false;
			}
			
		}else{
			return false;
		}
	}

}
