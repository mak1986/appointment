package software.engineering.appointment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.InvalidPropertiesFormatException;
import java.util.Observer;
import java.util.Properties;

public class Manager {
	private EventList eventList = new EventList();
	private Properties prop = new Properties();
	private DataSource dataSource;
	public Manager(DataSource dataSource) throws InvalidPropertiesFormatException, FileNotFoundException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		this.dataSource = dataSource;
		prop.loadFromXML(new FileInputStream("config.xml"));
		eventList.addAll((ArrayList<Event>) dataSource.getEvents());
	}
	public void addAppointment(String aDate, String aDescription, String aType) throws ParseException{
		this.eventList.add(aDate, aDescription, aType);
	}
	public void addToDo(String aDescription) throws ParseException{
		this.eventList.add(aDescription);
	}
	public void addToDo(String aDate, String aDescription) throws ParseException{
		this.eventList.add(aDate, aDescription,"todo");
	}
	public void saveEvents() {
		this.dataSource.writeEvents(this.eventList.getEvents());
	}
	public EventList getEventList() {
		return this.eventList;
	}
	public EventList getEventsOn(String aDate) throws ParseException {
		return this.eventList.getEventsOn(aDate);
	}
	public void addObserver(Observer observer){
		eventList.addObserver(observer);
	}

}
