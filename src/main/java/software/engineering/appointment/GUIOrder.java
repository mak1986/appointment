package software.engineering.appointment;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import java.awt.Color;
import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;

public class GUIOrder extends JFrame implements Observer{
	private Manager manager;
	private JTextArea textArea;
	private Comparator<? super Event> comparator;
	private String header;

	public GUIOrder(String title,Comparator<Event> comparator){
		try {
			manager = new Manager(new FileSource());
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.header = title;
		this.comparator = comparator;
		this.setup();
		this.createLabel();
		this.createTextArea();
	}
	private void setup() {
		setTitle(this.header);
		getContentPane().setLayout(null);
		setVisible(true);
		setSize(400, 400);
	}
	private void createLabel() {
		JLabel lblOrderByDate = new JLabel(this.header);
		lblOrderByDate.setBounds(112, 10, 175, 15);
		getContentPane().add(lblOrderByDate);
	}
	private void createTextArea() {
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 36, 365, 315);
		getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setFont(new Font("Calibri", Font.PLAIN, 11));
		textArea.setForeground(Color.GREEN);
		textArea.setBackground(Color.DARK_GRAY);
		scrollPane.setViewportView(textArea);
	}
	public static void main(String[] args){
		new GUIOrderByDate();
	}
	@Override
	public void update(Observable observable, Object arg1) {
		ArrayList<Event> events = ((EventList)observable).getEvents();
		Collections.sort(events,this.comparator);
		String result = "";
		for(Event event:events){
			result += event.getFormatedDate()+" "+event.getDescription()+"\n";
		}
		textArea.setText(result);
	}

}
