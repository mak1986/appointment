package software.engineering.appointment;

import java.text.ParseException;

public class Daily extends Appointment {

	public Daily(String aDate, String aDescription) throws ParseException {
		super(aDate, aDescription);
	}
}
