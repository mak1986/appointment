package software.engineering.appointment;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;

public class EventList extends Observable {
	private ArrayList<Event> events = new ArrayList<Event>();
	
	public void addAll(ArrayList<Event> list){
		this.events.addAll(list);
	}
	public void add(String aDate, String aDescription,String aType) throws ParseException{
		String type;
		Event event = null;
		type = aType.toLowerCase();
		type = type.replace(" ", "");
		switch(type){
			case "onetime":
				event = new OneTime(aDate, aDescription);
				break;
			case "daily":
				event = new Daily(aDate, aDescription);
				break;
			case "weekly":
				event = new Weekly(aDate, aDescription);
				break;
			case "monthly":
				event = new Monthly(aDate, aDescription);
				break;
			case "todo":
				event = new ToDo(aDate,aDescription);
				break;
		}
		this.events.add(event);
		this.setChanged();
		this.notifyObservers();
	}

	public void add(String aDesription) throws ParseException{
		this.events.add(new ToDo(aDesription));
		this.setChanged();
		this.notifyObservers();
	}
	public void add(Event event){
		this.events.add(event);
	}
	public int getNumEvents(){
		return this.events.size();
	}
	public Event getEvent(int index){
		return this.events.get(index);
	}
	public EventList getEventsOn(String aDate) throws ParseException {
		EventList newEvents = new EventList();
		for (Event event : this.events) {
			int year = Integer.parseInt(aDate.substring(6, 8));
			int month = Integer.parseInt(aDate.substring(3, 5));
			int day = Integer.parseInt(aDate.substring(0, 2));
			//System.out.println(year+" "+month+" "+day+" "+appointment.occursOn(year, month, day));
			if(event.occursOn(year, month, day)){
				newEvents.add(event);
			}
		}
		return newEvents;
	}
	public String toString(){
		String result = "\n<<< My current appointment Book >>>\n\n";
		if(this.getNumEvents()>0){
			for (Event event : this.events) {
			    result += event.toString();
			}
		}else{
			result += "No appointments.";
		}
		//result += "\n";
		return result;
	}
	public ArrayList<Event> getEvents(){
		return this.events;
	}

	
}
