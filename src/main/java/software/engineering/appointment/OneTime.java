package software.engineering.appointment;

import java.text.ParseException;

public class OneTime extends Appointment{

	public OneTime(String aDate, String aDescription) throws ParseException {
		super(aDate, aDescription);
	}

	@Override
	public boolean occursOn(int year, int month, int day) throws ParseException {
		return DATEFORMAT.parse(day+"/"+month+"/"+year).equals(this.getDateNoTime());
	}
}
