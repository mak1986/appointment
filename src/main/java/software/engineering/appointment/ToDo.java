package software.engineering.appointment;

import java.text.ParseException;

public class ToDo extends Event{

	private boolean done = false;
	
	public ToDo(String aDescription) throws ParseException {
		super(aDescription);
	}
	public ToDo(String aDate, String aDescription) throws ParseException{
		super(aDate, aDescription);
	}
	public boolean isDone(){
		return this.done;
	}
	
	@Override
	public boolean occursOn(int year, int month, int day) throws ParseException{
		if(this.getDate()==null){
			return true;
		}else if(DATEFORMAT.parse(DATEFORMAT.format(this.getDate())).equals(DATEFORMAT.parse(day+"/"+month+"/"+year))){
			return true;
		}else{
			return false;
		}
	}
	public String toString(){
		if(this.getDate()==null){
			return "ToDo[description="+this.getDescription()+"]";
		}else{
			return "ToDo[date="+this.getFormatedDate()+", description="+this.getDescription()+"]\n";
		}
	}

}
