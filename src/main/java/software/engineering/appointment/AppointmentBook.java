package software.engineering.appointment;

import java.text.ParseException;
import java.util.ArrayList;

public class AppointmentBook {
	private ArrayList<Appointment> appointments = new ArrayList<Appointment>();
	
	public void addAll(ArrayList<Appointment> list){
		this.appointments.addAll(list);
	}
	public void add(String aDate, String aDescription,String aType) throws ParseException{
		String type;
		Appointment appointment = null;
		type = aType.toLowerCase();
		switch(type){
			case "onetime":
				appointment = new OneTime(aDate, aDescription);
				break;
			case "daily":
				appointment = new Daily(aDate, aDescription);
				break;
			case "weekly":
				appointment = new Weekly(aDate, aDescription);
				break;
			case "monthly":
				appointment = new Monthly(aDate, aDescription);
				break;
		}
		this.appointments.add(appointment);
	}
	public void add(Appointment appointment){
		this.appointments.add(appointment);
	}
	public int getNumAppointments(){
		return this.appointments.size();
	}
	public Appointment getAppointment(int index){
		return this.appointments.get(index);
	}
	public AppointmentBook search(String aDate) throws ParseException {
		AppointmentBook newAppointments = new AppointmentBook();
		for (Appointment appointment : this.appointments) {
			int year = Integer.parseInt(aDate.substring(6, 8));
			int month = Integer.parseInt(aDate.substring(3, 5));
			int day = Integer.parseInt(aDate.substring(0, 2));
			//System.out.println(year+" "+month+" "+day+" "+appointment.occursOn(year, month, day));
			if(appointment.occursOn(year, month, day)){
				newAppointments.add(appointment);
			}
		}
		return newAppointments;
	}
	public String toString(){
		String result = "\n<<< My current appointment Book >>>\n\n";
		if(this.getNumAppointments()>0){
			for (Appointment appointment : this.appointments) {
			    result += appointment.toString()+"\n";
			}
		}else{
			result += "No appointments.";
		}
		//result += "\n";
		return result;
	}

}
