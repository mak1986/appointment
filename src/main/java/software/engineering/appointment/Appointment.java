package software.engineering.appointment;

import java.text.ParseException;
import java.util.Date;

public class Appointment extends Event{
	
	public Appointment(String aDate, String aDescription) throws ParseException{
		super(aDate,aDescription);
	}

	public String toString(){
		return "Appointment[date="+this.getFormatedDate()+", description="+this.getDescription()+"]\n";
	}
}
