package software.engineering.appointment;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.InvalidPropertiesFormatException;
import java.util.Scanner;

public class AppointmentConsole implements UI {
	static final Scanner SCANNER = new Scanner(System.in);
	private Manager manager;
	
	public AppointmentConsole() throws InvalidPropertiesFormatException, FileNotFoundException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException{
		this.manager = new Manager(new FileSource());
	}
	public void run() {
		while(true){
			String action = this.getActions();
			if(action.equals("add")){
				String type = this.getTypes();
				if(type.equals("a")){
					this.chooseOption("add appointment");
				}else{
					this.chooseOption("add to do");
				}
			}else if(action.equals("search")){
				this.chooseOption("search");
			}else if(action.equals("exit")){
				this.chooseOption("exit");
				break;
			}else{
				this.chooseOption("show all");
			}	
		}
	}
	private void addAppointment() {
		System.out.println("\n<<< Add appointment >>>(dd/MM/yy HH:mm)");
		System.out.print("Date: ");
		String aDate = SCANNER.nextLine();
		System.out.print("Description: ");
		String aDescription = SCANNER.nextLine();
		System.out.print("Type (Onetime, Daily, Weekly, Monthly):");
		String aType = SCANNER.nextLine();
		try {
			this.manager.addAppointment(aDate, aDescription, aType);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void addToDo(){
		System.out.println("\n<<< Add To Do >>>(dd/MM/yy)");
		System.out.print("Date: ");
		String aDate = SCANNER.nextLine();
		System.out.print("Description: ");
		String aDescription = SCANNER.nextLine();
		if(aDate.equals("")){
			try {
				this.manager.addToDo(aDescription);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else{
			try {
				this.manager.addToDo(aDate, aDescription);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
	}
	private String getActions() {
		System.out.println("\n<<< Action >>>");
		System.out.println("Action (Show all, Add, Search, Exit):");
		String action = SCANNER.nextLine();
		action = action.toLowerCase();
		return action;
	}
	private String getTypes(){
		System.out.println("\n<<< Type >>>");
		System.out.println("Type (Appointment (A), To Do (T)):");
		String type = SCANNER.nextLine();
		type = type.toLowerCase();
		return type;
	}
	private void chooseOption(String option) {
		switch (option) {
		case "show all":
			this.showAll();
			break;
		case "add appointment":
			this.addAppointment();
			break;
		case "add to do":
			this.addToDo();
			break;
		case "search":
			this.search();
			break;
		case "exit":
			this.exit();
			break;
		}
	}

	private void exit() {
		this.manager.saveEvents();
		System.out.println("\n<<< Thank you >>>");
	}
	private void showAll() {
		System.out.print(this.manager.getEventList());
	}

	private void search() {
		System.out.println("\n<<< Search >>>");
		System.out.print("Date: ");
		String aDate = SCANNER.nextLine();
		try {
			System.out.print(this.manager.getEventsOn(aDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws InvalidPropertiesFormatException, FileNotFoundException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {
		new AppointmentConsole().run();
	}


}
