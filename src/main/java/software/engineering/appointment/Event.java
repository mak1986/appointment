package software.engineering.appointment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public abstract class Event {
	private Date date;
	private String description;
	public static final DateFormat DATETIMEFORMAT = new SimpleDateFormat("dd/MM/yy HH:mm", new Locale("en", "US"));
	public static final DateFormat DATEFORMAT = new SimpleDateFormat("dd/MM/yy", new Locale("en", "US"));
	
	public Event(String aDate, String aDescription) throws ParseException{
		if(aDate.length()<=12){
			this.date = DATEFORMAT.parse(aDate);
		}else{
			this.date = DATETIMEFORMAT.parse(aDate);
		}
		this.description = aDescription;
	}
	public Event(String aDescription) throws ParseException{
		this.date = null;
		this.description = aDescription;
	}
	public String getDescription(){
		return this.description;
	}
	public Date getDate(){
		return this.date;
	}
	public String getFormatedDate(){
		if(this.date!=null){
			return DATETIMEFORMAT.format(this.date);
		}else{
			return "";
		}
	}
	public Date getDateNoTime() throws ParseException{
		return DATEFORMAT.parse(DATEFORMAT.format(this.getDate()));
	}
	public boolean occursOn(int year, int month, int day) throws ParseException{
		return !this.getDateNoTime().after(DATEFORMAT.parse(day+"/"+month+"/"+year));
	};
}
