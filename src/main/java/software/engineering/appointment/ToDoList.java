package software.engineering.appointment;

import java.text.ParseException;
import java.util.ArrayList;

public class ToDoList {
	private ArrayList<ToDo> toDoList = new ArrayList<ToDo>();
	
	public void addAll(ArrayList<ToDo> list){
		this.toDoList.addAll(list);
	}
	public void add(String aDate, String aDescription) throws ParseException{
		this.toDoList.add(new ToDo(aDate, aDescription));
	}
	public void add(String aDescription) throws ParseException{
		this.toDoList.add(new ToDo(aDescription));
	}
	public void add(ToDo toDo){
		this.toDoList.add(toDo);
	}
	public int getNumToDos(){
		return this.toDoList.size();
	}
	public ToDo getToDo(int index){
		return this.toDoList.get(index);
	}
	public ToDoList getToDosOn(String aDate) throws ParseException {
		ToDoList newToDoList = new ToDoList();
		for (ToDo toDo : this.toDoList) {
			int year = Integer.parseInt(aDate.substring(6, 8));
			int month = Integer.parseInt(aDate.substring(3, 5));
			int day = Integer.parseInt(aDate.substring(0, 2));
			//System.out.println(year+" "+month+" "+day+" "+toDo.occursOn(year, month, day));
			if(toDo.occursOn(year, month, day)){
				newToDoList.add(toDo);
			}
		}
		return newToDoList;
	}
	public String toString(){
		String result = "";
		if(this.getNumToDos()>0){
			for (ToDo toDo : this.toDoList) {
			    result += toDo.toString()+"\n";
			}
		}else{
			result += "No to dos.";
		}
		result += "\n";
		return result;
	}

}
